var scotchApp = angular.module('photoApp', ['ngRoute']);

    // configure our routes
    scotchApp.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'home.html',
                controller  : 'mainController'
            })

            // route for the about page
            .when('/color', {
                templateUrl : 'color.html',
                controller  : 'colorController'
            })

            // route for the contact page
            .when('/monochrome', {
                templateUrl : 'monochrome.html',
                controller  : 'monochromeController'
            });

    });

    // create the controller and inject Angular's $scope
    scotchApp.controller('mainController', function($scope) {
       $('header').show();
       $('#nav-about').show();
       $('#nav-contact').show();
       $('#nav-home').hide();
       $('#footer').show();
    });

    scotchApp.controller('colorController', function($scope) {
      
      $('header').hide();
      $('#nav-about').hide();
       $('#nav-contact').hide();
       $('#nav-home').show();
       $('#footer').hide();
$.getJSON("https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=5380c448d88d8bd66521ad03709da2d5&user_id=142479169@N02&format=json&photoset_id=72157668011550356&jsoncallback=?", function (data) {
console.log(data);
   $.each(data.photoset.photo, function (i,set) {
      // list="";
      // img="";
      // list="<a href='https://www.google.com/' title="
      // list+=set.title
      // list+=" data-gallery=''>"
      // img="<img src='https://farm'"+set.farm+"'.staticflickr.com/'"+set.server+"'/'"+set.id+"'_'"+set.secret+"'_t.jpg'>"
      // list=list+img+"</a>";
      // //list="<a href='https://www.google.com/' title="+title+"data-gallery="">"+"<img src='https://farm'"+farm+"'.staticflickr.com/'"+server+"'/'"+id+"'_'"+secret+"'_t.jpg'></a>";
      //  $("#links").append(list);
 var img = $("<img/>").attr("src", "https://farm"+set.farm+".staticflickr.com/"+set.server+"/"+set.id+"_"+set.secret+"_n.jpg")
  var list = $("<a/>").attr("title", set.title)    
          .attr("data-gallery","")
          .attr("href","https://farm"+set.farm+".staticflickr.com/"+set.server+"/"+set.id+"_"+set.secret+"_h.jpg")
          .append(img);
          $("#links").append(list);
    });

});


    });

    scotchApp.controller('monochromeController', function($scope) {
      $('header').hide();
         $('#nav-about').hide();
       $('#nav-contact').hide();
       $('#nav-home').show();
       $('#footer').hide();

$.getJSON("https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=5380c448d88d8bd66521ad03709da2d5&user_id=142479169@N02&format=json&photoset_id=72157667447654220&jsoncallback=?", function (data) {

// $scope.names = data.photoset.photo;


  
    $.each(data.photoset.photo, function (i,set) {
      // list="";
      // img="";
      // list="<a href='https://www.google.com/' title="
      // list+=set.title
      // list+=" data-gallery=''>"
      // img="<img src='https://farm'"+set.farm+"'.staticflickr.com/'"+set.server+"'/'"+set.id+"'_'"+set.secret+"'_t.jpg'>"
      // list=list+img+"</a>";
      // //list="<a href='https://www.google.com/' title="+title+"data-gallery="">"+"<img src='https://farm'"+farm+"'.staticflickr.com/'"+server+"'/'"+id+"'_'"+secret+"'_t.jpg'></a>";
      //  $("#links").append(list);
 var img = $("<img/>").attr("src", "https://farm"+set.farm+".staticflickr.com/"+set.server+"/"+set.id+"_"+set.secret+"_n.jpg")
  var list = $("<a/>").attr("title", set.title)    
          .attr("data-gallery","")
          .attr("href","https://farm"+set.farm+".staticflickr.com/"+set.server+"/"+set.id+"_"+set.secret+"_h.jpg")
          .append(img);
          $("#links").append(list);
    });
   

});

// $http.jsonp('https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=91d90a2d4da3ecd7a6ebc2e6060bdc0b&user_id=139931615@N05&format=json&photoset_id=72157661856874394&jsoncallback=?')
// .success(function (data) {
//    console.log(data);
// }
// });
  //       $http({
  //   method : "jsonp",
  //   url : "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=91d90a2d4da3ecd7a6ebc2e6060bdc0b&user_id=139931615@N05&format=json&photoset_id=72157661856874394&?callback=jsonFlickrApi"
  // }).then(function mySucces(data, status, headers, config) {
      



  //   }, function myError(response) {
  //     console.log(response);
  // });




    });